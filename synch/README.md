# CCLUB - ColumnFlow Synch parser
This folder contains the a simple configurable parser that takes as input an output NTuple of the CCLUB framework and dumps a CSV file in the format needed for the synchronisation with the ColumnFlow framework.

The CSV files needs to be compatible with the automated synchronisation tool developed at UHH: [sync](https://github.com/uhh-cms/sync).

## Setup
This parser does not need any specific environment setup (for the moment). 

If this parser is used inside the CCLUB environment (i.e. after running the `source setupo.sh` of CCLUB), it will work out of the box.

If this parse is used outside of the CCLUB environment, any 'standard' and installation of `python3` should have all the necessary ingredients. In case the default python installation on your cluster does not have them, you can install/update with the following: `pip3 install --upgrade optparse pandas uproot yaml`.

## Use
To use this parser:

1. Set the configuration yaml to contain:

    * the base folder where the CCLUB NTuples are stored;
    * the `runPeriod`, `sample`, `category`, and `version` of the CCLUB production for which the CSV wants to be created;
    * the specific `file` for which to dump the CSV;
    * set the events for which the CSV should be filled;
    * set the variables to dump in the CSV (the naming convention here is the used in the CCLUBB framework, which is gonna be transformed into the one of the sync tool. The renaming convention is defined in [this dictionary]()).

2. Run the comand `python3 CCLUB2CSVparser.py --cfg cfg_<year>_<period>.yaml`.

3. Use the produced CSV file with the sync tool and following its isntructions.