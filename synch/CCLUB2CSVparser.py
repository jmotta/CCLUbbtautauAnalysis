from optparse import OptionParser
import pandas as pd
import uproot
import yaml
import sys
import os

renameDict = {"event"           : "event",
              "run"             : "run",
              "luminosityBlock" : "lumi",
              "pairType"        : "channel_id",
              "isOS"            : "is_os",
              "bjet1_pt_nom"    : "jet1_pt",
              "bjet1_eta"       : "jet1_eta",
              "bjet1_phi"       : "jet1_phi",
              "bjet1_mass_nom"  : "jet1_mass",
              "bjet2_pt_nom"    : "jet2_pt",
              "bjet2_eta"       : "jet2_eta",
              "bjet2_phi"       : "jet2_phi",
              "bjet2_mass_nom"  : "jet2_mass",
              "dau1_pt"         : "lep1_pt",
              "dau1_eta"        : "lep1_eta",
              "dau1_phi"        : "lep1_phi",
              "dau1_charge"     : "lep1_charge",
              "dau2_pt"         : "lep2_pt",
              "dau2_eta"        : "lep2_eta",
              "dau2_phi"        : "lep2_phi",
              "dau2_charge"     : "lep2_charge"
             }

def loadToDf(file, branches):
    dfs = []
    with uproot.open(file) as f:
        tree = f["Events"] 
        df = tree.arrays(branches, library="pd")
        dfs.append(df)

    if "event" in branches:
        df['event'] = df['event'].astype('int64')
    if "run" in branches:
        df['run'] = df['run'].astype('int32')
    if "luminosityBlock" in branches:
        df['luminosityBlock'] = df['luminosityBlock'].astype('int32')

    return pd.concat(dfs, ignore_index=True)

def filterEvents(df, events):
    if len(events) > 0:
        eventsSelStr = ""
        for evt in events:
            eventsSelStr += f"event == {evt} or "
        eventsSelStr = eventsSelStr[:-4]

        return df.query(eventsSelStr)

    else:
        return df

#######################################################################
######################### SCRIPT BODY #################################
#######################################################################

if __name__ == "__main__" :
    parser = OptionParser()
    parser.add_option("--cfg", dest="cfg", help="YAML config file for dump")
    (options, args) = parser.parse_args()

    with open(options.cfg, 'r') as file:
        cfg = yaml.safe_load(file)

    branches = cfg["variables"]
    
    inputStr = (cfg["inputBase"]+'/'
                + cfg["runPeriod"]+'/'
                + cfg["sample"]+'/'
                + cfg["category"]+'/'
                + cfg["version"]+'/'
                + cfg["file"])
    DF = loadToDf(inputStr, branches)
    
    filteredDF = filterEvents(DF, cfg["test_events"])
    filteredDF = filteredDF.rename(columns=renameDict)

    os.system('mkdir -p ./' + cfg["runPeriod"] + '/'
                            + cfg["sample"] + '/'
                            + cfg["category"] + '/'
                            + cfg["version"])
    
    outputStr = (cfg["runPeriod"] + '/'
                 + cfg["sample"] + '/'
                 + cfg["category"] + '/'
                 + cfg["version"] + '/'
                 + cfg["file"].replace('.root', '.csv'))
    filteredDF.to_csv(outputStr, index=False)
