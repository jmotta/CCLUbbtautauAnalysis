echo "##############################################################################################################################################################################"
echo "Creating local directory ./test/test_PreprocessRDF_results for CI tests outputs"
mkdir ./test/test_PreprocessRDF_results
if [ ! -d "./test/test_PreprocessRDF_results" ]; then
  echo "./test/test_PreprocessRDF_results directory does not exist!"
  return "1"
fi

echo "##############################################################################################################################################################################"
echo "MC 2022 preEE"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2022_preEE --category-name base --modules-file run3_2022_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 10 --branch 0 --dataset-name GluGlutoHHto2B2Tau_SM_2022_preEE_100events

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2022 preEE"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2022_preEE --category-name base --modules-file run3_2022_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2022_preEE_100events

echo ""
echo "##############################################################################################################################################################################"
echo "MC 2022 postEE"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2022_postEE --category-name base --modules-file run3_2022_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_SM_2022_postEE_100events

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2022 postEE"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2022_postEE --category-name base --modules-file run3_2022_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2022_postEE_100events

echo ""
echo "##############################################################################################################################################################################"
echo "MC 2023 preBPix"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2023_preBPix --category-name base --modules-file run3_2023_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_SM_2023_preBPIX_100events

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2023 preBPix"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2023_preBPix --category-name base --modules-file run3_2023_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2023_preBPIX_100events

echo ""
echo "##############################################################################################################################################################################"
echo "MC 2023 postBPix"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2023_postBPix --category-name base --modules-file run3_2023_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_SM_2023_postBPIX_100events

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2023 postBPix"
echo ""
law run PreprocessRDF --version picoTest --config-name run3_2023_postBPix --category-name base --modules-file run3_2023_preprocess_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2023_postBPIX_100events
