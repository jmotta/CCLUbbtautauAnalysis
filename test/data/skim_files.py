# ----------------------------------------------------------------- #
# Skim files to only 100 events:
#  - Download the NANOAOD files locally (see commands below)
#  - Run `python3 skim_files.py` to skim to only 100 events
#
# List of files downloaded (dataset, file, command for each period):
#  # MC 2022 preEE
#  /GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_LHEweights_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM
#  davs://srm.ciemat.es:2880/pnfs/ciemat.es/data/cms/prod/store/mc/Run3Summer22NanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_LHEweights_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/2540000/01886557-3793-4af5-bb7f-84b52987ccab.root
#  gfal-copy davs://srm.ciemat.es:2880/pnfs/ciemat.es/data/cms/prod/store/mc/Run3Summer22NanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_LHEweights_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/2540000/01886557-3793-4af5-bb7f-84b52987ccab.root ggHH_2022_preEE.root
#  
#  # MC 2022 postEE
#  /GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM
#  davs://webdav.recas.ba.infn.it:8443/cms/store/mc/Run3Summer22EENanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v3/2540000/236af134-c3dd-4a83-92d3-09aa867a6f61.root
#  gfal-copy davs://webdav.recas.ba.infn.it:8443/cms/store/mc/Run3Summer22EENanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v3/2540000/236af134-c3dd-4a83-92d3-09aa867a6f61.root ggHH_2022_postEE.root
#  
#  # MC 2023 preBPIX
#  /GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer23NanoAODv13-133X_mcRun3_2023_realistic_ForNanov13_v1-v3/NANOAODSIM
#  davs://cmsdcache-kit-disk.gridka.de:2880/pnfs/gridka.de/cms/disk-only/store/mc/Run3Summer23NanoAODv13/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/133X_mcRun3_2023_realistic_ForNanov13_v1-v3/60000/020c6527-9b4e-4494-8bf7-75a2489e0bfa.root
#  gfal-copy davs://cmsdcache-kit-disk.gridka.de:2880/pnfs/gridka.de/cms/disk-only/store/mc/Run3Summer23NanoAODv13/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/133X_mcRun3_2023_realistic_ForNanov13_v1-v3/60000/020c6527-9b4e-4494-8bf7-75a2489e0bfa.root ggHH_2023_preBPIX.root
#  
#  # MC 2023 postBPIX
#  /GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer23BPixNanoAODv13-133X_mcRun3_2023_realistic_postBPix_ForNanov13_v2-v2/NANOAODSIM
#  davs://t2-xrdcms.lnl.infn.it:2880/pnfs/lnl.infn.it/data/cms/store/mc/Run3Summer23BPixNanoAODv13/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/133X_mcRun3_2023_realistic_postBPix_ForNanov13_v2-v2/2820000/2a8277da-56d7-462e-bf9d-7e3a255393c9.root
#  gfal-copy davs://t2-xrdcms.lnl.infn.it:2880/pnfs/lnl.infn.it/data/cms/store/mc/Run3Summer23BPixNanoAODv13/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/133X_mcRun3_2023_realistic_postBPix_ForNanov13_v2-v2/2820000/2a8277da-56d7-462e-bf9d-7e3a255393c9.root ggHH_2023_postBPIX.root
#  
#  # Data 2022 preEE
#  /Tau/Run2022C-22Sep2023-v1/NANOAOD
#  davs://t2-xrdcms.lnl.infn.it:2880/pnfs/lnl.infn.it/data/cms/store/data/Run2022C/Tau/NANOAOD/22Sep2023-v1/40000/21d3d4ae-b427-4abb-9aaf-f0f87ef8b61f.root
#  gfal-copy davs://t2-xrdcms.lnl.infn.it:2880/pnfs/lnl.infn.it/data/cms/store/data/Run2022C/Tau/NANOAOD/22Sep2023-v1/40000/21d3d4ae-b427-4abb-9aaf-f0f87ef8b61f.root tau_2022_preEE.root
#  
#  # Data 2022 postEE
#  /Tau/Run2022E-22Sep2023-v1/NANOAOD
#  davs://webdav.recas.ba.infn.it:8443/cms/store/data/Run2022E/Tau/NANOAOD/22Sep2023-v1/30000/117b7d01-bd3a-4f16-aaa2-9188b50a17d3.root
#  gfal-copy davs://webdav.recas.ba.infn.it:8443/cms/store/data/Run2022E/Tau/NANOAOD/22Sep2023-v1/30000/117b7d01-bd3a-4f16-aaa2-9188b50a17d3.root tau_2022_postEE.root
#  
#  # Data 2023 preBPIX
#  /Tau/Run2023C-22Sep2023_v1-v2/NANOAOD
#  davs://webdav.recas.ba.infn.it:8443/cms/store/data/Run2022E/Tau/NANOAOD/22Sep2023-v1/30000/117b7d01-bd3a-4f16-aaa2-9188b50a17d3.root
#  gfal-copy davs://webdav.recas.ba.infn.it:8443/cms/store/data/Run2022E/Tau/NANOAOD/22Sep2023-v1/30000/117b7d01-bd3a-4f16-aaa2-9188b50a17d3.root tau_2023_preBPIX.root
#  
#  # Data 2023 postBPIX
#  /Tau/Run2023D-22Sep2023_v1-v1/NANOAOD
#  davs://storage01.lcg.cscs.ch:2880/pnfs/lcg.cscs.ch/cms/trivcat/store/data/Run2023D/Tau/NANOAOD/22Sep2023_v1-v1/2530000/7dd8ad6a-51dc-4632-8642-404e42818876.root
#  gfal-copy davs://storage01.lcg.cscs.ch:2880/pnfs/lcg.cscs.ch/cms/trivcat/store/data/Run2023D/Tau/NANOAOD/22Sep2023_v1-v1/2530000/7dd8ad6a-51dc-4632-8642-404e42818876.root tau_2023_postBPIX.root
#
# ----------------------------------------------------------------- #

import ROOT

files = [
    ['ggHH_2022_preEE.root'   , 'GluGlutoHHto2B2Tau_SM_2022_preEE_100events.root'   ],
    ['ggHH_2022_postEE.root'  , 'GluGlutoHHto2B2Tau_SM_2022_postEE_100events.root'  ],
    ['ggHH_2023_preBPIX.root' , 'GluGlutoHHto2B2Tau_SM_2023_preBPIX_100events.root' ],
    ['ggHH_2023_postBPIX.root', 'GluGlutoHHto2B2Tau_SM_2023_postBPIX_100events.root'],
    ['tau_2022_preEE.root'    , 'Tau_2022_preEE_100events.root'                     ],
    ['tau_2022_postEE.root'   , 'Tau_2022_postEE_100events.root'                    ],
    ['tau_2023_preBPIX.root'  , 'Tau_2023_preBPIX_100events.root'                   ],
    ['tau_2023_postBPIX.root' , 'Tau_2023_postBPIX_100events.root'                  ],
]

for file in files:
    rdf = ROOT.RDataFrame('Events', file[0])
    rdf_100 = rdf.Range(100)
    rdf_100.Snapshot('Events', file[1])

