from analysis_tools import ObjectCollection, Category, Process, Dataset, Feature, Systematic
from analysis_tools.utils import DotDict
from analysis_tools.utils import join_root_selection as jrs
from plotting_tools import Label
from collections import OrderedDict

from cmt.config.base_config import Config as cmt_base_config
from cmt.base_tasks.base import Task

class Config(cmt_base_config):
    def __init__(self, *args, **kwargs):
        self.channels = self.add_channels()
        super(Config, self).__init__(*args, **kwargs)

    def join_selection_channels(self, selection):
        return jrs([jrs(jrs(selection[ch.name], op="and"), ch.selection, op="and")
            for ch in self.channels], op="or")

    def combine_selections_per_channel(self, selection1, selection2):
        selection = DotDict()
        for channel in selection1:
            selection[channel] = jrs(jrs(selection1[channel], op="and"),
                jrs(selection2[channel], op="and"), op="or")
        return selection

    def add_channels(self):
        channels = [
            Category("mutau", Label("#tau_{#mu}#tau_{h}"),
                     selection="pass_triggers && pairType == 0",
                     skip_processes=["data_egamma", "data_tau", "data_jet", "data_parkinghh"]
                    ),
            Category("etau", Label("#tau_{e}#tau_{h}"),
                     selection="pass_triggers && pairType == 1",
                     skip_processes=["data_muon", "data_tau", "data_jet", "data_parkinghh"]
                    ),
            Category("tautau", Label("#tau_{h}#tau_{h}"),
                     selection="pass_triggers && pairType == 2",
                     skip_processes=["data_egamma", "data_muon"]
                    ),
            Category("mumu", Label("#mu#mu"),
                     selection="pass_triggers && pairType == 3",
                     skip_processes=["data_egamma", "data_tau", "data_jet", "data_parkinghh", "data_parkingvbf"]
                    ),
            Category("ee", Label("ee"),
                     selection="pass_triggers && pairType == 4",
                     skip_processes=["data_muon", "data_tau", "data_jet", "data_parkinghh", "data_parkingvbf"]
                    ),
            Category("emu", Label("e#mu"),
                     selection="pass_triggers && pairType == 5",
                     skip_processes=["data_tau", "data_jet", "data_parkinghh", "data_parkingvbf"]
                    ),
        ]
        return ObjectCollection(channels)

    def add_regions(self):
        selection = OrderedDict()
        region_names = ["Signal region", "OS inv. iso", "SS iso", "SS inv. iso"]

        selection["os_iso"] = {
            "mutau":  ["isOS == 1",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium],
            "etau":   ["isOS == 1",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium],
            "tautau": ["isOS == 1",
                       "dau1_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium,
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium],
            "mumu":   ["isOS == 1"],
            "ee"  :   ["isOS == 1"],
            "emu" :   ["isOS == 1"],
        }
        selection["os_inviso"] = {
            "mutau":  ["isOS == 1",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.VVVLoose,
                       "dau2_tauIdVSjet < %s" % self.tauId_algo_wps.vsjet.Medium],
            "etau":   ["isOS == 1",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.VVVLoose,
                       "dau2_tauIdVSjet < %s" % self.tauId_algo_wps.vsjet.Medium],
            "tautau": ["isOS == 1",
                       "dau1_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium,
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.VVVLoose,
                       "dau2_tauIdVSjet < %s" % self.tauId_algo_wps.vsjet.Medium],
            "mumu":   ["isOS == 1"],
            "ee"  :   ["isOS == 1"],
            "emu" :   ["isOS == 1"],
        }
        selection["ss_iso"] = {
            "mutau":  ["isOS == 0",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium],
            "etau":   ["isOS == 0",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium],
            "tautau": ["isOS == 0",
                       "dau1_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium,
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium],
            "mumu":   ["isOS == 0"],
            "ee"  :   ["isOS == 0"],
            "emu" :   ["isOS == 0"],
        }
        selection["ss_inviso"] = {
            "mutau":  ["isOS == 0",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.VVVLoose,
                       "dau2_tauIdVSjet < %s" % self.tauId_algo_wps.vsjet.Medium],
            "etau":   ["isOS == 0",
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.VVVLoose,
                       "dau2_tauIdVSjet < %s" % self.tauId_algo_wps.vsjet.Medium],
            "tautau": ["isOS == 0",
                       "dau1_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.Medium,
                       "dau2_tauIdVSjet >= %s" % self.tauId_algo_wps.vsjet.VVVLoose,
                       "dau2_tauIdVSjet < %s" % self.tauId_algo_wps.vsjet.Medium],
            "mumu":   ["isOS == 0"],
            "ee"  :   ["isOS == 0"],
            "emu" :   ["isOS == 0"],
        }
        regions = []
        for channel in self.channels:
            regions.append(channel)
            regions.append(
                Category(channel.name + "_os", label=channel.label,
                    selection=" (%s) && (isOS == 1)" % channel.selection)
            )
        for ikey, key in enumerate(selection):
            regions.append(Category(key, label=Label(region_names[ikey]),
                selection=self.join_selection_channels(selection[key])))
            for channel in self.channels:
                regions.append(Category("_".join([channel.name, key]),
                    label=Label(", ".join([channel.label.root + " channel", region_names[ikey]])),
                    selection=jrs(channel.selection,
                        jrs(selection[key][channel.name], op="and"), op="and")))
        return ObjectCollection(regions)

    def add_categories(self, **kwargs):
        fully_reject = ["pairType == -31415"]

        sel = DotDict()

        #---------------------------------------------------
        # bTag selections
        btag = kwargs.pop("btag", "bjet{}_btag")
        df = lambda i, op, wp: "{} {} {}".format(btag.format(i), op, self.btag_algo_wps[wp])
        sel["btag"] = DotDict(
            oneMedium = [jrs(df(1, ">", "medium"), df(2, ">", "medium"), op="or")],
            oneLoose  = [jrs(df(1, ">", "loose"), df(2, ">", "loose"), op="or")],
            twoMedium = [df(1, ">", "medium"), df(2, ">", "medium")],
            twoLoose  = [df(1, ">", "loose"), df(2, ">", "loose")],
            oneLoose_oneMedium = [jrs(jrs(df(1, ">", "medium"), df(2, ">", "loose"), op="and"),
                                      jrs(df(1, ">", "loose"), df(2, ">", "medium"), op="and"),
                                      op="or")],
            # placeholders for FatJet WPs
            fatXbbVsQCD_none = ["fatbjet_btag > 0.0"],
            fatXbbVsQCD_loose = ["fatbjet_btag > 0.25"],
            fatXbbVsQCD_medium = ["fatbjet_btag > 0.5"]
        )

        #---------------------------------------------------
        # Invariant mass selections
        mass_resolved_sel = ["(({{Htt_svfit_mass}} - 129.) * ({{Htt_svfit_mass}} - 129.)/ (53. * 53.)"
                             " + ({{Hbb_mass}} - 169.) * ({{Hbb_mass}} - 169.) / (145. * 145.)) <= 1"]

        mass_boosted_sel = ["(({{Htt_svfit_mass}} - 128.) * ({{Htt_svfit_mass}} - 128.) / (60. * 60.)"
                            " + ({{Hbb_mass}} - 159.) * ({{Hbb_mass}} - 159.) / (94. * 94.)) <= 1"]

        mass_resolved_sel_inv = ["(({{Htt_svfit_mass}} - 129.) * ({{Htt_svfit_mass}} - 129.)/ (53. * 53.)"
                                 " + ({{Hbb_mass}} - 169.) * ({{Hbb_mass}} - 169.) / (145. * 145.)) > 1"]

        mass_boosted_sel_inv = ["(({{Htt_svfit_mass}} - 128.) * ({{Htt_svfit_mass}} - 128.) / (60. * 60.)"
                                " + ({{Hbb_mass}} - 159.) * ({{Hbb_mass}} - 159.) / (94. * 94.)) > 1"]

        #---------------------------------------------------
        # VBF selections
        vbf = ["hasVBFAK4", "{{VBFjj_mass}} > 500", "abs({{VBFjj_deltaEta}}) > 3"]
        vbf_tight = vbf + ["isVBFtrigger", "{{VBFjj_mass}} > 800",
                           "{{vbfjet1_pt}} > 140", "{{vbfjet2_pt}} > 60"]

        #---------------------------------------------------
        # Jet-based-categories selections
        sel["res2b"] = DotDict({
            ch: (["hasResolvedAK4",
                  "!("+jrs(vbf, op="and")+")"] + # orthogonal to VBF
                  sel.btag.twoMedium + mass_resolved_sel
                )
            for ch in self.channels.names()
        })
        sel["res2b_combined"] = self.join_selection_channels(sel["res2b"])

        sel["boosted"] = DotDict({
            ch: (["hasBoostedAK8",
                  "!isQuadJetTrigger", "!isTauTauJetTrigger",
                  "!("+jrs(vbf, op="and")+")",      # orthogonal to VBF
                  "!("+sel["res2b_combined"]+")"] + # orthogonal to res2b
                  mass_boosted_sel + sel.btag.fatXbbVsQCD_none
                )
            for ch in self.channels.names()
        })
        sel["boosted_combined"] = self.join_selection_channels(sel["boosted"])

        sel["res1b"] = DotDict({
            ch: (["hasResolvedAK4",
                  "!("+jrs(vbf, op="and")+")",      # orthogonal to VBF
                  "!("+sel["boosted_combined"]+")", # orthogonal to boosted
                  "!("+sel["res2b_combined"]+")"] + # orthogonal to res2b
                  sel.btag.oneLoose_oneMedium + mass_resolved_sel
                )
            for ch in self.channels.names()
        })
        sel["res1b_combined"] = self.join_selection_channels(sel["res1b"])

        sel["vbf_2b"] = DotDict({
            ch: ([jrs(vbf, op="and")] +
                 sel.btag.twoMedium
                )
            for ch in self.channels.names()
        })
        sel["vbf_2b_combined"] = self.join_selection_channels(sel["vbf_2b"])

        sel["vbf_res2b"] = DotDict({
            ch: (["hasResolvedAK4",
                  jrs(vbf, op="and")] +
                 sel.btag.twoMedium
                )
            for ch in self.channels.names()
        })
        sel["vbf_res2b_combined"] = self.join_selection_channels(sel["vbf_res2b"])

        sel["vbf_boosted"] = DotDict({
            ch: (["hasBoostedAK8",
                  "!isQuadJetTrigger", "!isTauTauJetTrigger",
                  jrs(vbf, op="and"),
                  "!("+sel["vbf_res2b_combined"]+")"] + # orthogonal to vbf_res2b
                 sel.btag.fatXbbVsQCD_none
                )
            for ch in self.channels.names()
        })
        sel["vbf_boosted_combined"] = self.join_selection_channels(sel["vbf_boosted"])

        sel["vbf_res1b"] = DotDict({
            ch: (["hasResolvedAK4",
                  jrs(vbf, op="and"),
                  "!("+sel["vbf_boosted_combined"]+")", # orthogonal to vbf_boosted
                  "!("+sel["vbf_res2b_combined"]+")"] + # orthogonal to vbf_res2b
                 sel.btag.oneLoose_oneMedium
                )
            for ch in self.channels.names()
        })
        sel["vbf_res1b_combined"] = self.join_selection_channels(sel["vbf_res1b"])

        #---------------------------------------------------
        # VBF selections as implemented in Run2
        sel["vbf_loose"] = DotDict({
            ch: [jrs(vbf, op="and")]
            for ch in self.channels.names()
        })
        sel["vbf_loose_combined"] = self.join_selection_channels(sel["vbf_loose"])

        sel["vbf_tight"] = DotDict(
            mutau  = fully_reject, # |
            etau   = fully_reject, # |
            mumu   = fully_reject, # | -> channels not used in "vbf_tight"
            ee     = fully_reject, # |
            emu    = fully_reject, # |
            tautau = vbf_tight + sel.btag.oneMedium,
        )
        sel["vbf_tight_combined"] = self.join_selection_channels(sel["vbf_tight"])

        sel["vbf"] = self.combine_selections_per_channel(sel.vbf_tight, sel.vbf_loose)
        sel["vbf_combined"] = self.join_selection_channels(sel.vbf)

        #---------------------------------------------------
        # CR selections
        sel["ttbar_invertedMassCR"] = DotDict({
            ch: (["hasResolvedAK4",
                  "!("+jrs(vbf, op="and")+")"] + # orthogonal to VBF
                  sel.btag.twoMedium + mass_resolved_sel_inv + mass_boosted_sel_inv
                )
            for ch in self.channels.names()
        })
        sel["ttbar_invertedMassCR_combined"] = self.join_selection_channels(sel["ttbar_invertedMassCR"])

        sel["DY_CR"]="{{Htt_mass}} >= 70 && {{Htt_mass}} <= 110 && {{PuppiMET_pt}} < 45"

        #---------------------------------------------------
        # Analysis combined selections
        sel["baseline"]          = "pass_triggers && (hasResolvedAK4 || hasBoostedAK8)"
        sel["baseline_resolved"] = "pass_triggers && hasResolvedAK4"
        sel["baseline_boosted"]  = "pass_triggers && hasBoostedAK8"
        sel["anaysis_channels"]  = "pairType >= 0 && pairType <= 2"
        sel["control_channels"]  = "pairType >= 3 && pairType <= 5"

        #---------------------------------------------------
        # Actual Category Definitions
        categories = [
            # Dummy category
            Category("dum", "dummy category",
                     selection="event == 74472670"),

            # Baseline categories
            Category("base", "base category",
                     selection="event >= 0"),

            Category("baseline", "Baseline",
                     selection=jrs(sel["baseline"], sel["anaysis_channels"], op="and")),

            Category("control", "Control channels",
                     selection=jrs(sel["baseline"], sel["control_channels"], op="and")),

            Category("baseline_bResolved", "Baseline - Resolved b-jets",
                     selection=jrs(sel["baseline_resolved"], sel["anaysis_channels"], op="and")),

            Category("baseline_bBoosted", "Baseline - Boosted b-jets",
                     selection=jrs(sel["baseline_boosted"], sel["anaysis_channels"], op="and")),

            Category("control_bResolved", "Control channels - Resolved b-jets",
                     selection=jrs(sel["baseline_resolved"], sel["control_channels"], op="and")),

            Category("control_bBoosted", "Control channels - Boosted b-jets",
                     selection=jrs(sel["baseline_boosted"], sel["control_channels"], op="and")),

            # Analysis categories
            Category("res1b", "Resolved 1b category",
                     selection=jrs(sel["baseline_resolved"], sel["res1b_combined"], op="and")),

            Category("res2b", "Resolved 2b category",
                     selection=jrs(sel["baseline_resolved"], sel["res2b_combined"], op="and")),

            Category("boosted", "Boosted category",
                     selection=jrs(sel["baseline_boosted"], sel["boosted_combined"], op="and")),

            Category("vbf_2b", "VBF 2b category",
                     selection=jrs(sel["baseline"], sel["vbf_2b_combined"], op="and")),

            Category("vbf_res1b", "VBF - Resolved 1b category",
                     selection=jrs(sel["baseline_resolved"], sel["vbf_res1b_combined"], op="and")),

            Category("vbf_res2b", "VBF - Resolved 2b category",
                     selection=jrs(sel["baseline_resolved"], sel["vbf_res2b_combined"], op="and")),

            Category("vbf_boosted", "VBF - Boosted category",
                     selection=jrs(sel["baseline_boosted"], sel["vbf_boosted_combined"], op="and")),

            # VBF categories as implemented in Run2
            Category("vbf_loose", "VBF - Loose category",
                     selection=jrs("pass_triggers", sel["vbf_loose_combined"], op="and")),

            Category("vbf_tight", "VBF - Tight category",
                     selection=jrs("pass_triggers", sel["vbf_tight_combined"], op="and")),

            Category("vbf", "VBF - Loose + Tight category",
                     selection=jrs("pass_triggers", sel["vbf_combined"], op="and")),

            # CR categories
            Category("DY_CR_res1b", "DY CR - Resolved 1b category",
                     selection=jrs(jrs(sel["baseline_resolved"],sel["DY_CR"], op="and"),
                                   sel["res1b_combined"], op="and")),

            Category("DY_CR_res2b", "DY CR - Resolved 2b category",
                     selection=jrs(jrs(sel["baseline_resolved"],sel["DY_CR"], op="and"),
                                   sel["res2b_combined"], op="and")),

            Category("DY_CR_boosted", "DY CR - Boosted category",
                     selection=jrs(jrs(sel["baseline_boosted"],sel["DY_CR"], op="and"),
                                   sel["boosted_combined"], op="and")),

            Category("ttbar_invertedMassCR", "t#bar{t} CR - Inverted mass cut",
                     selection=jrs(sel["baseline_resolved"],
                                   sel["ttbar_invertedMassCR_combined"], op="and")),
        ]
        return ObjectCollection(categories)

    def add_processes(self):
        processes = [
            Process("data", Label("data"), color=(0, 0, 0), isData=True),
            Process("data_tau", Label("data_tau"), color=(0, 0, 0), parent_process="data", isData=True),
            Process("data_muon", Label("data_muon"), color=(0, 0, 0), parent_process="data", isData=True),
            Process("data_egamma", Label("data_egamma"), color=(0, 0, 0), parent_process="data", isData=True),
            Process("data_jet", Label("data_jet"), color=(0, 0, 0), parent_process="data", isData=True),
            Process("data_parkinghh", Label("data_parkinghh"), color=(0, 0, 0), parent_process="data", isData=True),
            Process("data_parkingvbf", Label("data_parkingvbf"), color=(0, 0, 0), parent_process="data", isData=True),
            # 2022 preEE data samples
            Process("Tau_2022C", Label("Tau_2022C"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2022D", Label("Tau_2022D"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("SingleMuon_2022C", Label("SingleMuon_2022C"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon_2022C", Label("Muon_2022C"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon_2022D", Label("Muon_2022D"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("EGamma_2022C", Label("EGamma_2022C"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma_2022D", Label("EGamma_2022D"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("JetHT_2022C", Label("JetHT_2022C"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET_2022C", Label("JetMET_2022C"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET_2022D", Label("JetMET_2022D"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            # 2022 postEE data samples
            Process("Tau_2022E", Label("Tau_2022E"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2022F", Label("Tau_2022F"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2022G", Label("Tau_2022G"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Muon_2022E", Label("Muon_2022E"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon_2022F", Label("Muon_2022F"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon_2022G", Label("Muon_2022G"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("EGamma_2022E", Label("EGamma_2022E"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma_2022F", Label("EGamma_2022F"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma_2022G", Label("EGamma_2022G"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("JetMET_2022E", Label("JetMET_2022E"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET_2022F", Label("JetMET_2022F"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET_2022G", Label("JetMET_2022G"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            # 2023 preBPix data samples
            Process("Tau_2023Cv1", Label("Tau_2023Cv1"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2023Cv2", Label("Tau_2023Cv2"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2023Cv3", Label("Tau_2023Cv3"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2023Cv4", Label("Tau_2023Cv4"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Muon0_2023Cv1", Label("Muon0_2023Cv1"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon0_2023Cv2", Label("Muon0_2023Cv2"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon0_2023Cv3", Label("Muon0_2023Cv3"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon0_2023Cv4", Label("Muon0_2023Cv4"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon1_2023Cv1", Label("Muon1_2023Cv1"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon1_2023Cv2", Label("Muon1_2023Cv2"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon1_2023Cv3", Label("Muon1_2023Cv3"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon1_2023Cv4", Label("Muon1_2023Cv4"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("EGamma0_2023Cv1", Label("EGamma0_2023Cv1"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma0_2023Cv2", Label("EGamma0_2023Cv2"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma0_2023Cv3", Label("EGamma0_2023Cv3"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma0_2023Cv4", Label("EGamma0_2023Cv4"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma1_2023Cv1", Label("EGamma1_2023Cv1"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma1_2023Cv2", Label("EGamma1_2023Cv2"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma1_2023Cv3", Label("EGamma1_2023Cv3"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma1_2023Cv4", Label("EGamma1_2023Cv4"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("JetMET0_2023Cv1", Label("JetMET0_2023Cv1"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET0_2023Cv2", Label("JetMET0_2023Cv2"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET0_2023Cv3", Label("JetMET0_2023Cv3"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET0_2023Cv4", Label("JetMET0_2023Cv4"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET1_2023Cv1", Label("JetMET1_2023Cv1"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET1_2023Cv2", Label("JetMET1_2023Cv2"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET1_2023Cv3", Label("JetMET1_2023Cv3"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET1_2023Cv4", Label("JetMET1_2023Cv4"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("ParkingHH_2023Cv3", Label("ParkingHH_2023Cv3"), color=(0, 0, 0), parent_process="data_parkinghh", isData=True),
            Process("ParkingHH_2023Cv4", Label("ParkingHH_2023Cv4"), color=(0, 0, 0), parent_process="data_parkinghh", isData=True),
            Process("ParkingVBF0_2023Cv3", Label("ParkingVBF0_2023Cv3"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF0_2023Cv4", Label("ParkingVBF0_2023Cv4"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF1_2023Cv3", Label("ParkingVBF1_2023Cv3"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF1_2023Cv4", Label("ParkingVBF1_2023Cv4"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF2_2023Cv3", Label("ParkingVBF2_2023Cv3"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF2_2023Cv4", Label("ParkingVBF2_2023Cv4"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF3_2023Cv3", Label("ParkingVBF3_2023Cv3"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF3_2023Cv4", Label("ParkingVBF3_2023Cv4"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF4_2023Cv3", Label("ParkingVBF4_2023Cv3"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF5_2023Cv4", Label("ParkingVBF5_2023Cv4"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF6_2023Cv3", Label("ParkingVBF6_2023Cv3"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF7_2023Cv4", Label("ParkingVBF7_2023Cv4"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            # 2023 postBPix data samples
            Process("Tau_2023Dv1", Label("Tau_2023Dv1"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Tau_2023Dv2", Label("Tau_2023Dv2"), color=(0, 0, 0), parent_process="data_tau", isData=True),
            Process("Muon0_2023Dv1", Label("Muon0_2023Dv1"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon0_2023Dv2", Label("Muon0_2023Dv2"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon1_2023Dv1", Label("Muon1_2023Dv1"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("Muon1_2023Dv2", Label("Muon1_2023Dv2"), color=(0, 0, 0), parent_process="data_muon", isData=True),
            Process("EGamma0_2023Dv1", Label("EGamma0_2023Dv1"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma0_2023Dv2", Label("EGamma0_2023Dv2"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma1_2023Dv1", Label("EGamma1_2023Dv1"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("EGamma1_2023Dv2", Label("EGamma1_2023Dv2"), color=(0, 0, 0), parent_process="data_egamma", isData=True),
            Process("JetMET0_2023Dv1", Label("JetMET0_2023Dv1"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET0_2023Dv2", Label("JetMET0_2023Dv2"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET1_2023Dv1", Label("JetMET1_2023Dv1"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("JetMET1_2023Dv2", Label("JetMET1_2023Dv2"), color=(0, 0, 0), parent_process="data_jet", isData=True),
            Process("ParkingHH_2023Dv1", Label("ParkingHH_2023Dv1"), color=(0, 0, 0), parent_process="data_parkinghh", isData=True),
            Process("ParkingHH_2023Dv2", Label("ParkingHH_2023Dv2"), color=(0, 0, 0), parent_process="data_parkinghh", isData=True),
            Process("ParkingVBF0_2023Dv1", Label("ParkingVBF0_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF0_2023Dv2", Label("ParkingVBF0_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF1_2023Dv1", Label("ParkingVBF1_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF1_2023Dv2", Label("ParkingVBF1_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF2_2023Dv1", Label("ParkingVBF2_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF2_2023Dv2", Label("ParkingVBF2_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF3_2023Dv1", Label("ParkingVBF3_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF3_2023Dv2", Label("ParkingVBF3_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF4_2023Dv1", Label("ParkingVBF4_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF4_2023Dv2", Label("ParkingVBF4_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF5_2023Dv1", Label("ParkingVBF5_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF5_2023Dv2", Label("ParkingVBF5_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF6_2023Dv1", Label("ParkingVBF6_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF6_2023Dv2", Label("ParkingVBF6_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF7_2023Dv1", Label("ParkingVBF7_2023Dv1"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),
            Process("ParkingVBF7_2023Dv2", Label("ParkingVBF7_2023Dv2"), color=(0, 0, 0), parent_process="data_parkingvbf", isData=True),

            # DY
            Process("dy", Label("dy"), color=(122, 33, 221)),
            Process("DYto2L-2Jets_MLL-50"                 , Label("DY incl")       , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_ext1"            , Label("DY incl")       , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_0J"              , Label("DY 0J")         , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_1J"              , Label("DY 1J")         , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_2J"              , Label("DY 2J")         , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-40to100_1J" , Label("DY 40to100 1J") , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-40to100_2J" , Label("DY 40to100 2J") , isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-100to200_1J", Label("DY 100to200 1J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-100to200_2J", Label("DY 100to200 2J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-200to400_1J", Label("DY 200to400 1J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-200to400_2J", Label("DY 200to400 2J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-400to600_1J", Label("DY 400to600 1J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-400to600_2J", Label("DY 400to600 2J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-600_1J"     , Label("DY 600toInf 1J"), isDY=True, color=(122, 33, 221), parent_process="dy"),
            Process("DYto2L-2Jets_MLL-50_PTLL-600_2J"     , Label("DY 600toInf 2J"), isDY=True, color=(122, 33, 221), parent_process="dy"),

            Process("ttbar", Label("ttbar"), color=(228, 37, 54)),
            Process("TTto2L2Nu", Label("TTto2L2Nu"), color=(228, 37, 54), parent_process="ttbar"),
            Process("TTto2L2Nu_ext1", Label("TTto2L2Nu_ext1"), color=(228, 37, 54), parent_process="ttbar"),
            Process("TTtoLNu2Q", Label("TTtoLNu2Q"), color=(228, 37, 54), parent_process="ttbar"),
            Process("TTtoLNu2Q_ext1", Label("TTtoLNu2Q_ext1"), color=(228, 37, 54), parent_process="ttbar"),
            Process("TTto4Q", Label("TTto4Q"), color=(228, 37, 54), parent_process="ttbar"),
            Process("TTto4Q_ext1", Label("TTto4Q_ext1"), color=(228, 37, 54), parent_process="ttbar"),

            Process("singleh", Label("singleh"), color=(248, 156, 32)),
            Process("GluGluHToTauTau", Label("GluGluHToTauTau"), color=(248, 156, 32), parent_process="singleh"),
            Process("GluGluHto2B", Label("GluGluHto2B"), color=(248, 156, 32), parent_process="singleh"),
            Process("VBFHToTauTau", Label("VBFHToTauTau"), color=(248, 156, 32), parent_process="singleh"),
            Process("VBFHto2B", Label("VBFHto2B"), color=(248, 156, 32), parent_process="singleh"),
            Process("WminusHTo2Tau", Label("WminusHTo2Tau"), color=(248, 156, 32), parent_process="singleh"),
            Process("WplusHTo2Tau", Label("WplusHTo2Tau"), color=(248, 156, 32), parent_process="singleh"),
            Process("ZHto2Tau", Label("ZHto2Tau"), color=(248, 156, 32), parent_process="singleh"),
            Process("ZH_Hto2B_Zto2L", Label("ZH_Hto2B_Zto2L"), color=(248, 156, 32), parent_process="singleh"),
            Process("ZH_Hto2B_Zto2L_ext1", Label("ZH_Hto2B_Zto2L_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("ZH_Hto2B_Zto2Q", Label("ZH_Hto2B_Zto2Q"), color=(248, 156, 32), parent_process="singleh"),
            Process("ZH_Hto2B_Zto2Q_ext1", Label("ZH_Hto2B_Zto2Q_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("WminusH_Hto2B_Wto2Q", Label("WminusH_Hto2B_Wto2Q"), color=(248, 156, 32), parent_process="singleh"),
            Process("WminusH_Hto2B_Wto2Q_ext1", Label("WminusH_Hto2B_Wto2Q_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("WminusH_Hto2B_WtoLNu", Label("WminusH_Hto2B_WtoLNu"), color=(248, 156, 32), parent_process="singleh"),
            Process("WminusH_Hto2B_WtoLNu_ext1", Label("WminusH_Hto2B_WtoLNu_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("WplusH_Hto2B_Wto2Q", Label("WplusH_Hto2B_Wto2Q"), color=(248, 156, 32), parent_process="singleh"),
            Process("WplusH_Hto2B_Wto2Q_ext1", Label("WplusH_Hto2B_Wto2Q_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("WplusH_Hto2B_WtoLNu", Label("WplusH_Hto2B_WtoLNu"), color=(248, 156, 32), parent_process="singleh"),
            Process("WplusH_Hto2B_WtoLNu_ext1", Label("WplusH_Hto2B_WtoLNu_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("ggZH_Hto2B_Zto2L", Label("ggZH_Hto2B_Zto2L"), color=(248, 156, 32), parent_process="singleh"),
            Process("ggZH_Hto2B_Zto2L_ext1", Label("ggZH_Hto2B_Zto2L_ext1"), color=(248, 156, 32), parent_process="singleh"),
            Process("ggZH_Hto2B_Zto2Q", Label("ggZH_Hto2B_Zto2Q"), color=(248, 156, 32), parent_process="singleh"),
            Process("ggZH_Hto2B_Zto2Q_ext1", Label("ggZH_Hto2B_Zto2Q_ext1"), color=(248, 156, 32), parent_process="singleh"),

            Process("others", Label("others"), color=(156, 156, 161)),
            Process("WW", Label("WW"), color=(156, 156, 161), parent_process="others"),
            Process("WWto2L2Nu", Label("WWto2L2Nu"), color=(156, 156, 161), parent_process="others"),
            Process("WWto2L2Nu_ext1", Label("WWto2L2Nu_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("WWtoLNu2Q", Label("WWtoLNu2Q"), color=(156, 156, 161), parent_process="others"),
            Process("WWtoLNu2Q_ext1", Label("WWtoLNu2Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("WWto4Q", Label("WWto4Q"), color=(156, 156, 161), parent_process="others"),
            Process("WWto4Q_ext1", Label("WWto4Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("ZZ", Label("ZZ"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto2L2Nu", Label("ZZto2L2Nu"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto2L2Nu_ext1", Label("ZZto2L2Nu_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto2L2Q", Label("ZZto2L2Q"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto2L2Q_ext1", Label("ZZto2L2Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto2Nu2Q", Label("ZZto2Nu2Q"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto2Nu2Q_ext1", Label("ZZto2Nu2Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto4L", Label("ZZto4L"), color=(156, 156, 161), parent_process="others"),
            Process("ZZto4L_ext1", Label("ZZto4L_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("WZto2L2Q", Label("WZto2L2Q"), color=(156, 156, 161), parent_process="others"),
            Process("WZtoLNu2Q", Label("WZtoLNu2Q"), color=(156, 156, 161), parent_process="others"),
            Process("WZto3LNu", Label("WZto3LNu"), color=(156, 156, 161), parent_process="others"),
            Process("WWW", Label("WWW"), color=(156, 156, 161), parent_process="others"),
            Process("WWZ", Label("WWZ"), color=(156, 156, 161), parent_process="others"),
            Process("WZZ", Label("WZZ"), color=(156, 156, 161), parent_process="others"),
            Process("ZZZ", Label("ZZZ"), color=(156, 156, 161), parent_process="others"),
            Process("TWminusto2L2Nu", Label("TWminusto2L2Nu"), color=(156, 156, 161), parent_process="others"),
            Process("TWminusto2L2Nu_ext1", Label("TWminusto2L2Nu_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("TWminustoLNu2Q", Label("TWminustoLNu2Q"), color=(156, 156, 161), parent_process="others"),
            Process("TWminustoLNu2Q_ext1", Label("TWminustoLNu2Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("TWminusto4Q", Label("TWminusto4Q"), color=(156, 156, 161), parent_process="others"),
            Process("TWminusto4Q_ext1", Label("TWminusto4Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("TbarWplusto2L2Nu", Label("TbarWplusto2L2Nu"), color=(156, 156, 161), parent_process="others"),
            Process("TbarWplusto2L2Nu_ext1", Label("TbarWplusto2L2Nu_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("TbarWplustoLNu2Q", Label("TbarWplustoLNu2Q"), color=(156, 156, 161), parent_process="others"),
            Process("TbarWplustoLNu2Q_ext1", Label("TbarWplustoLNu2Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("TbarWplusto4Q", Label("TbarWplusto4Q"), color=(156, 156, 161), parent_process="others"),
            Process("TbarWplusto4Q_ext1", Label("TbarWplusto4Q_ext1"), color=(156, 156, 161), parent_process="others"),
            Process("TBbarQ_t-channel", Label("TBbarQ_t-channel"), color=(156, 156, 161), parent_process="others"),
            Process("TbarBQ_t-channel", Label("TbarBQ_t-channel"), color=(156, 156, 161), parent_process="others"),
            Process("TBbartoLplusNuBbar-s-channel", Label("TBbartoLplusNuBbar-s-channel"), color=(156, 156, 161), parent_process="others"),
            Process("TbarBtoLminusNuB-s-channel", Label("TbarBtoLminusNuB-s-channel"), color=(156, 156, 161), parent_process="others"),
            Process("TTHto2B", Label("TTHto2B"), color=(156, 156, 161), parent_process="others"),
            Process("TTHtoNon2B", Label("TTHtoNon2B"), color=(156, 156, 161), parent_process="others"),
            Process("TTWH", Label("TTWH"), color=(156, 156, 161), parent_process="others"),
            Process("TTWW", Label("TTWW"), color=(156, 156, 161), parent_process="others"),
            Process("TTWZ", Label("TTWZ"), color=(156, 156, 161), parent_process="others"),
            Process("TTZH", Label("TTZH"), color=(156, 156, 161), parent_process="others"),
            Process("TTZZ", Label("TTZZ"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets"                  , Label("WJets incl")       , color=(156, 156, 161), parent_process="others", filterVPt=True, maxVPt=40),
            Process("WtoLNu-2Jets_PTLNu-40to100_1J" , Label("WJets 40to100 1J") , color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-40to100_2J" , Label("WJets 40to100 2J") , color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-100to200_1J", Label("WJets 100to200 1J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-100to200_2J", Label("WJets 100to200 2J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-200to400_1J", Label("WJets 200to400 1J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-200to400_2J", Label("WJets 200to400 2J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-400to600_1J", Label("WJets 400to600 1J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-400to600_2J", Label("WJets 400to600 2J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-600_1J"     , Label("WJets 600toInf 1J"), color=(156, 156, 161), parent_process="others"),
            Process("WtoLNu-2Jets_PTLNu-600_2J"     , Label("WJets 600toInf 2J"), color=(156, 156, 161), parent_process="others"),
            # Currently not used
            #Process("WtoLNu-2Jets_0J"             , Label("WtoLNu-2Jets_0J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-2Jets_1J"             , Label("WtoLNu-2Jets_1J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-2Jets_2J"             , Label("WtoLNu-2Jets_2J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets"                , Label("WtoLNu-4Jets")                , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_ext1"           , Label("WtoLNu-4Jets_ext1")           , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_1J"             , Label("WtoLNu-4Jets_1J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_2J"             , Label("WtoLNu-4Jets_2J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_3J"             , Label("WtoLNu-4Jets_3J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_4J"             , Label("WtoLNu-4Jets_4J")             , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-120to200"  , Label("WtoLNu-4Jets_MLNu-120to200")  , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-200to400"  , Label("WtoLNu-4Jets_MLNu-200to400")  , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-400to800"  , Label("WtoLNu-4Jets_MLNu-400to800")  , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-800to1500" , Label("WtoLNu-4Jets_MLNu-800to1500") , color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-1500to2500", Label("WtoLNu-4Jets_MLNu-1500to2500"), color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-2500to4000", Label("WtoLNu-4Jets_MLNu-2500to4000"), color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-4000to6000", Label("WtoLNu-4Jets_MLNu-4000to6000"), color=(156, 156, 161), parent_process="others"),
            #Process("WtoLNu-4Jets_MLNu-6000"      , Label("WtoLNu-4Jets_MLNu-6000")      , color=(156, 156, 161), parent_process="others"),

            Process("qqhh", Label("qqhh"), color=(87, 144, 252)),
            Process("VBFHHto2B2Tau_CV-1_C2V-1_C3-1", Label("VBFHHto2B2Tau_CV-1_C2V-1_C3-1"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-1_C2V-0_C3-1", Label("VBFHHto2B2Tau_CV-1_C2V-0_C3-1"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-1p74_C2V-1p37_C3-14p4", Label("VBFHHto2B2Tau_CV-1p74_C2V-1p37_C3-14p4"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m0p012_C2V-0p030_C3-10p2", Label("VBFHHto2B2Tau_CV-m0p012_C2V-0p030_C3-10p2"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m0p758_C2V-1p44_C3-m19p3", Label("VBFHHto2B2Tau_CV-m0p758_C2V-1p44_C3-m19p3"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m0p962_C2V-0p959_C3-m1p43", Label("VBFHHto2B2Tau_CV-m0p962_C2V-0p959_C3-m1p43"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m1p21_C2V-1p94_C3-m0p94", Label("VBFHHto2B2Tau_CV-m1p21_C2V-1p94_C3-m0p94"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m1p60_C2V-2p72_C3-m1p36", Label("VBFHHto2B2Tau_CV-m1p60_C2V-2p72_C3-m1p36"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m1p83_C2V-3p57_C3-m3p39", Label("VBFHHto2B2Tau_CV-m1p83_C2V-3p57_C3-m3p39"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),
            Process("VBFHHto2B2Tau_CV-m2p12_C2V-3p87_C3-m5p96", Label("VBFHHto2B2Tau_CV-m2p12_C2V-3p87_C3-m5p96"), color=(87, 144, 252), parent_process="qqhh", isSignal=True),

            Process("gghh", Label("gghh"), color=(0, 0, 0)),
            Process("GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00", Label("GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00"), color=(0, 0, 0), parent_process="gghh", isSignal=True),
            Process("GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00", Label("GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00"), color=(0, 0, 0), parent_process="gghh", isSignal=True),
            Process("GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00", Label("GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00"), color=(0, 0, 0), parent_process="gghh", isSignal=True),
            Process("GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00", Label("GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00"), color=(0, 0, 0), parent_process="gghh", isSignal=True),

            # For GitLab CI tests
            Process("ci_tests_gghh", Label("ci_tests_gghh"), color=(0, 0, 0)),
            Process("GluGlutoHHto2B2Tau_SM_2022_preEE_100events"   , Label("GluGlutoHHto2B2Tau_SM_2022_preEE_100events")   , color=(0, 0, 0), parent_process="ci_tests_gghh", isSignal=True),
            Process("GluGlutoHHto2B2Tau_SM_2022_postEE_100events"  , Label("GluGlutoHHto2B2Tau_SM_2022_postEE_100events")  , color=(0, 0, 0), parent_process="ci_tests_gghh", isSignal=True),
            Process("GluGlutoHHto2B2Tau_SM_2023_preBPIX_100events" , Label("GluGlutoHHto2B2Tau_SM_2023_preBPIX_100events") , color=(0, 0, 0), parent_process="ci_tests_gghh", isSignal=True),
            Process("GluGlutoHHto2B2Tau_SM_2023_postBPIX_100events", Label("GluGlutoHHto2B2Tau_SM_2023_postBPIX_100events"), color=(0, 0, 0), parent_process="ci_tests_gghh", isSignal=True),

            Process("ci_tests_tau", Label("ci_tests_tau"), color=(0, 0, 0)),
            Process("Tau_2022_preEE_100events"   , Label("Tau_2022_preEE_100events")   , color=(0, 0, 0), parent_process="ci_tests_tau", isData=True),
            Process("Tau_2022_postEE_100events"  , Label("Tau_2022_postEE_100events")  , color=(0, 0, 0), parent_process="ci_tests_tau", isData=True),
            Process("Tau_2023_preBPIX_100events" , Label("Tau_2023_preBPIX_100events") , color=(0, 0, 0), parent_process="ci_tests_tau", isData=True),
            Process("Tau_2023_postBPIX_100events", Label("Tau_2023_postBPIX_100events"), color=(0, 0, 0), parent_process="ci_tests_tau", isData=True),
        ]

        process_group_names = {
            "quicktest": [
                "data",
                "GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00"
            ],
            "allProcesses": [
                "data",
                "dy",
                "ttbar",
                "singleh",
                "others",
                "gghh",
                "qqhh"
            ],
            "mainProcesses": [
                "data",
                "DYto2L-2Jets_MLL-50",
                "WtoLNu-2Jets",
                "ttbar",
                "VBFHHto2B2Tau_CV-1_C2V-1_C3-1",
                "GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00"
            ],
            "ggf": [
                "GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00",
                "GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00",
                "GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00",
                "GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00",
            ],
            "ci_tests": [
                "ci_tests_gghh",
                "ci_tests_tau",
            ]
        }

        process_training_names = {
            "default": [
                "ggf_sm",
                "dy"
            ]
        }

        return ObjectCollection(processes), process_group_names, process_training_names

    def add_features(self):
        features = [
            Feature("jet_pt", "Jet_pt",
                        binning=(10, 50, 150),
                        x_title=Label("jet p_{T}"),
                        units="GeV"),

            # lepton features
            Feature("tau_pt", "Tau_pt",
                        binning=(75, 0, 150),
                        x_title=Label("#tau p_{T}"),
                        units="GeV"),
            Feature("tau_pt_tes", "Tau_pt_corr_Medium",
                        binning=(75, 0, 150),
                        x_title=Label("#tau p_{T}"),
                        units="GeV"),

            # bjet1 features
            Feature("bjet1_pt", "bjet1_pt",
                        binning=(13, 20, 150),
                        x_title=Label("b_{1} p_{T}"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("bjet1_eta", "bjet1_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("b_{1} #eta")),
            Feature("bjet1_phi", "bjet1_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("b_{1} #phi")),
            Feature("bjet1_mass", "bjet1_mass",
                        binning=(10, 50, 150),
                        x_title=Label("b_{1} m"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("bjet1_pNet", "bjet1_btag%s" % self.btag_algo,
                        binning=(20, 0, 1),
                        x_title=Label("b_{1} %s score" % self.btag_algo)),

            # bjet2 features
            Feature("bjet2_pt", "bjet2_pt",
                        binning=(13, 20, 150),
                        x_title=Label("b_{2} p_{T}"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("bjet2_eta", "bjet2_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("b_{2} #eta")),
            Feature("bjet2_phi", "bjet2_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("b_{2} #phi")),
            Feature("bjet2_mass", "bjet2_mass",
                        binning=(10, 50, 150),
                        x_title=Label("b_{2} m"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("bjet2_pNet", "bjet2_btag%s" % self.btag_algo,
                        binning=(20, 0, 1),
                        x_title=Label("b_{2} %s score" % self.btag_algo)),

            # fatbjet features
            Feature("fatbjet_pt", "fatbjet_pt",
                        binning=(10, 50, 150),
                        x_title=Label("Boosted b_{1}b_{2} p_{T}"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("fatbjet_eta", "fatbjet_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("Boosted b_{1}b_{2} #eta")),
            Feature("fatbjet_phi", "fatbjet_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("Boosted b_{1}b_{2} #phi")),
            Feature("fatbjet_mass", "fatbjet_mass",
                        binning=(10, 50, 150),
                        x_title=Label("Boosted b_{1}b_{2} m"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("fatbjet_pNet", "fatbjet_btag",
                        binning=(20, 0, 1),
                        x_title=Label("Boosted b_{1}b_{2} PNet score")),

            # central jets features
            Feature("ctjet1_pt", "Jet_pt.at(ctjet_indexes.at(0))",
                        binning=(10, 50, 150),
                        x_title=Label("add. central jet 1 p_{T}"),
                        units="GeV",
                        central="jet_smearing",
                        selection="ctjet_indexes.size() > 0"),
            Feature("ctjet1_eta", "Jet_eta.at(ctjet_indexes.at(0))",
                        binning=(20, -5., 5.),
                        x_title=Label("add. central jet 1 #eta"),
                        selection="ctjet_indexes.size() > 0"),
            Feature("ctjet1_phi", "Jet_phi.at(ctjet_indexes.at(0))",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("add. central jet 1 #phi"),
                        selection="ctjet_indexes.size() > 0"),
            Feature("ctjet1_mass", "Jet_mass.at(ctjet_indexes.at(0))",
                        binning=(10, 50, 150),
                        x_title=Label("add. central jet 1 m"),
                        units="GeV",
                        central="jet_smearing",
                        selection="ctjet_indexes.size() > 0"),

            # forward jets features
            Feature("fwjet1_pt", "Jet_pt.at(fwjet_indexes.at(0))",
                        binning=(10, 50, 150),
                        x_title=Label("add. forward jet 1 p_{T}"),
                        units="GeV",
                        central="jet_smearing",
                        selection="fwjet_indexes.size() > 0"),
            Feature("fwjet1_eta", "Jet_eta.at(fwjet_indexes.at(0))",
                        binning=(20, -5., 5.),
                        x_title=Label("add. forward jet 1 #eta"),
                        selection="fwjet_indexes.size() > 0"),
            Feature("fwjet1_phi", "Jet_phi.at(fwjet_indexes.at(0))",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("add. forward jet 1  #phi"),
                        selection="fwjet_indexes.size() > 0"),
            Feature("fwjet1_mass", "Jet_mass.at(fwjet_indexes.at(0))",
                        binning=(10, 50, 150),
                        x_title=Label("add. forward jet 1  m"),
                        units="GeV",
                        central="jet_smearing",
                        selection="fwjet_indexes.size() > 0"),

            Feature("bjet_difpt", "abs({{bjet1_pt}} - {{bjet2_pt}})",
                        binning=(10, 50, 150),
                        x_title=Label("bb #Delta p_{T}"),
                        units="GeV",
                        central="jet_smearing"),

            # dau1 features
            # the central dau1 values correspond to _corr/_corr_Medium for electrons/tau
            Feature("lep1_pt", "dau1_pt",
                        binning=(29, 10, 300),
                        x_title=Label("#tau_{1} p_{T}"),
                        units="GeV"),
                        # systematics=["tes"]),
            Feature("lep1_eta", "dau1_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("#tau_{1} #eta")),
            Feature("lep1_phi", "dau1_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("#tau_{1} #phi")),
            Feature("lep1_mass", "dau1_mass",
                        binning=(10, 50, 150),
                        x_title=Label("#tau_{1} m"),
                        units="GeV",
                        systematics=["tes"]),

            # dau2 features
            # the central dau2 values correspond to _corr_Medium for tau
            Feature("lep2_pt", "dau2_pt", #"dau2_pt_corr_Medium",
                        binning=(29, 10, 300),
                        x_title=Label("#tau_{2} p_{T}"),
                        units="GeV"),
                        # systematics=["tes"]),
            Feature("lep2_eta", "dau2_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("#tau_{2} #eta")),
            Feature("lep2_phi", "dau2_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("#tau_{2} #phi")),
            Feature("lep2_mass", "dau2_mass",
                        binning=(10, 50, 150),
                        x_title=Label("#tau_{2} m"),
                        units="GeV",
                        systematics=["tes"]),

            # dua1-dau2 deltaR
            Feature("deltaRtautau", "deltaRtautau",
                        binning=(25, 0, 5),
                        x_title=Label("#Delta R(#tau_{1},#tau_{2})")),
                        # systematics=["tes"]),

            # MET features
            Feature("MET_pt", "MET_pt",
                        binning=(15, 0, 150),
                        x_title=Label("MET p_{T}"),
                        units="GeV"),
                        # central="met_smearing"),
            Feature("PuppiMET_pt", "PuppiMET_pt",
                        binning=(15, 0, 150),
                        x_title=Label("PuppiMET p_{T}"),
                        units="GeV"),
                        # central="met_smearing"),
            Feature("PuppiMET_phi", "PuppiMET_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("PuppiMET #phi")),
                        # central="met_smearing"),

            # Hbb features
            Feature("Hbb_pt", "Hbb_pt",
                        binning=(30, 0, 300),
                        x_title=Label("H(b #bar{b}) p_{T}"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("Hbb_eta", "Hbb_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("H(b #bar{b}) #eta"),
                        central="jet_smearing"),
            Feature("Hbb_phi", "Hbb_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("H(b #bar{b}) #phi"),
                        central="jet_smearing"),
            Feature("Hbb_mass", "Hbb_mass",
                        binning=(30, 0, 300),
                        x_title=Label("H(b #bar{b}) m"),
                        units="GeV",
                        central="jet_smearing"),

            # Htt features
            Feature("Htt_pt", "Htt_pt", #"Htt_pt_corr_Medium_corr",
                        binning=(30, 0, 300),
                        x_title=Label("H(#tau^{+} #tau^{-}) p_{T}"),
                        units="GeV"),
                        # systematics=["tes"]),
            Feature("Htt_eta", "Htt_eta", #"Htt_eta_corr_Medium_corr",
                        binning=(20, -5., 5.),
                        x_title=Label("H(#tau^{+} #tau^{-}) #eta")),
                        # systematics=["tes"]),
            Feature("Htt_phi", "Htt_phi", #"Htt_phi_corr_Medium_corr",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("H(#tau^{+} #tau^{-}) #phi")),
                        #systematics=["tes"]),
            Feature("Htt_mass", "Htt_mass", #"Htt_mass_corr_Medium_corr",
                        binning=(30, 0, 300),
                        x_title=Label("H(#tau^{+} #tau^{-}) m"),
                        units="GeV"),
                        #systematics=["tes"]),

            # Htt (SVFit) features
            Feature("Htt_svfit_pt", "Htt_svfit_pt",
                        binning=(30, 0, 300),
                        x_title=Label("H(#tau^{+} #tau^{-}) p_{T} (SVFit)"),
                        units="GeV",
                        systematics=["tes"]),
            Feature("Htt_svfit_eta", "Htt_svfit_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("H(#tau^{+} #tau^{-}) #eta (SVFit)"),
                        systematics=["tes"]),
            Feature("Htt_svfit_phi", "Htt_svfit_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("H(#tau^{+} #tau^{-}) #phi (SVFit)"),
                        systematics=["tes"]),
            Feature("Htt_svfit_mass", "Htt_svfit_mass",
                        binning=(30, 0, 300),
                        x_title=Label("H(#tau^{+} #tau^{-}) m (SVFit)"),
                        units="GeV",
                        systematics=["tes"]),

            # HH features
            Feature("HH_pt", "HH_pt",
                        binning=(30, 0, 300),
                        x_title=Label("HH p_{T}"),
                        units="GeV",
                        systematics=["tes"]),
            Feature("HH_eta", "HH_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("HH #eta"),
                        systematics=["tes"]),
            Feature("HH_phi", "HH_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("HH #phi"),
                        systematics=["tes"]),
            Feature("HH_mass", "HH_mass",
                        binning=(50, 0, 1000),
                        x_title=Label("HH m"),
                        units="GeV",
                        systematics=["tes"]),

            # HH (SVFit) features
            Feature("HH_svfit_pt", "HH_svfit_pt",
                        binning=(30, 0, 300),
                        x_title=Label("HH p_{T} (SVFit)"),
                        units="GeV",
                        systematics=["tes"]),
            Feature("HH_svfit_eta", "HH_svfit_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("HH #eta (SVFit)")),
                        #systematics=["tes"]),
            Feature("HH_svfit_phi", "HH_svfit_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("HH #phi (SVFit)")),
                        #systematics=["tes"]),
            Feature("HH_svfit_mass", "HH_svfit_mass",
                        binning=(50, 0, 1000),
                        x_title=Label("HH m (SVFit)"),
                        units="GeV"),
                        #systematics=["tes"]),

            # HH KinFit features
            Feature("HH_kinfit_mass", "HH_kinfit_mass",
                        binning=(50, 0, 1000),
                        x_title=Label("HH m (Kin. Fit)"),
                        units="GeV",
                        systematics=["tes"]),
            Feature("HH_kinfit_chi2", "HH_kinfit_chi2",
                        binning=(30, 0, 10),
                        x_title=Label("HH #chi^2 (Kin. Fit)"),
                        systematics=["tes"]),

            # VBFjet1 features
            Feature("vbfjet1_pt", "vbfjet1_pt",
                        binning=(10, 50, 150),
                        x_title=Label("VBFjet1 p_{T}"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("vbfjet1_eta", "vbfjet1_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("VBFjet1 #eta")),
            Feature("vbfjet1_phi", "vbfjet1_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("VBFjet1 #phi")),
            Feature("vbfjet1_mass", "vbfjet1_mass",
                        binning=(10, 50, 150),
                        x_title=Label("VBFjet1 m"),
                        units="GeV",
                        central="jet_smearing"),

            # VBFjet2 features
            Feature("vbfjet2_pt", "vbfjet2_pt",
                        binning=(10, 50, 150),
                        x_title=Label("VBFjet2 p_{T}"),
                        units="GeV",
                        central="jet_smearing"),
            Feature("vbfjet2_eta", "vbfjet2_eta",
                        binning=(20, -5., 5.),
                        x_title=Label("VBFjet2 #eta")),
            Feature("vbfjet2_phi", "vbfjet2_phi",
                        binning=(20, -3.2, 3.2),
                        x_title=Label("VBFjet2 #phi")),
            Feature("vbfjet2_mass", "vbfjet2_mass",
                        binning=(10, 50, 150),
                        x_title=Label("VBFjet2 m"),
                        units="GeV",
                        central="jet_smearing"),

            # VBFjj features
            Feature("VBFjj_mass", "VBFjj_mass",
                        binning=(40, 0, 1000),
                        x_title=Label("VBFjj mass"),
                        units="GeV"),
            Feature("VBFjj_deltaEta", "VBFjj_deltaEta",
                        binning=(40, -8, 8),
                        x_title=Label("#Delta#eta(VBFjj)")),
            Feature("VBFjj_deltaPhi", "VBFjj_deltaPhi",
                        binning=(40, -6.4, 6.4),
                        x_title=Label("#Delta#phi(VBFjj)")),

            # Weights
            Feature("genWeight", "genWeight",
                        binning=(20, 0, 2),
                        x_title=Label("genWeight")),
            Feature("puWeight", "puWeight",
                        binning=(20, 0, 2),
                        x_title=Label("puWeight")),
                        # systematics=["pu"]),
            Feature("prescaleWeight", "prescaleWeight",
                        binning=(20, 0, 2),
                        x_title=Label("prescaleWeight")),
            Feature("trigSF", "trigSF",
                        binning=(20, 0, 2),
                        x_title=Label("trigSF")),

            # generator HH features
            Feature("genHH_mass", "genHH_mass",
                        binning=(100, 0, 2500),
                        x_title=Label("generator HH mass"),
                        units="GeV"),

            # LHE level features
            Feature("LHE_Vpt", "LHE_Vpt",
                    binning=(100, 0, 1000),
                    noData=True,
                    x_title=Label("LHE PtZ"),
                    units="GeV"),
            Feature("LHE_NpNLO", "LHE_NpNLO",
                    binning=(5, 0, 5),
                    noData=True,
                    x_title=Label("LHE Nb of partons at LO")),
        ]
        return ObjectCollection(features)

    def add_weights(self):
        weights = DotDict()
        weights.default = "1"

        return weights

    def add_systematics(self):
        systematics = []
        #     Systematic("tes", "_corr_Medium",
        #         affected_categories=self.categories.names(),
        #         module_syst_type="tau_syst"),
        # ]

        systematics = [
              Systematic("jet_smearing", "_nom")
        ]
        #     Systematic("met_smearing", ("MET", "MET_smeared")),
        #     Systematic("prefiring", "_Nom"),
        #     Systematic("prefiring_syst", "", up="_Up", down="_Dn"),
        #     Systematic("pu", "", up="Up", down="Down"),
        #     Systematic("empty", "", up="", down="")
        # ]

        return ObjectCollection(systematics)

    # other methods

    def get_channel_from_region(self, region):
        for sign in ["os", "ss"]:
            if sign in region.name:
                if region.name.startswith(sign):
                    return ""
                return region.name[:region.name.index("_%s" % sign)]
        return ""

    def get_qcd_regions(self, region, category, wp="", shape_region="os_inviso",
            signal_region_wp="os_iso", sym=False):
        # the region must be set and tagged os_iso
        if not region:
            raise Exception("region must not be empty")
        # if not region.has_tag("qcd_os_iso"):
        #     raise Exception("region must be tagged as 'qcd_os_iso' but isn't")

        # the category must be compatible with the estimation technique
        # if category.has_tag("qcd_incompatible"):
        #     raise Exception("category '{}' incompatible with QCD estimation".format(category.name))

        if wp != "":
            wp = "__" + wp

        # get other qcd regions
        prefix = region.name[:-len(signal_region_wp)]
        qcd_regions = {"ss_inviso": self.regions.get(prefix + "ss_inviso" + wp)}
        # for the inverted regions, allow different working points
        default_config = ["os_inviso", "ss_iso"]
        for key in default_config:
            region_name = (prefix + key + wp if key != "ss_iso"
                else prefix + "ss_" + signal_region_wp[len("os_"):])
            qcd_regions[key] = self.regions.get(region_name)

        if sym:
            qcd_regions["shape1"] = self.regions.get(prefix + shape_region + wp)
            qcd_regions["shape2"] = self.regions.get(
                prefix + "ss_" + signal_region_wp[len("os_"):])
        else:
            if shape_region == "os_inviso":
                qcd_regions["shape"] = self.regions.get(prefix + shape_region + wp)
            else:
                qcd_regions["shape"] = self.regions.get(
                    prefix + "ss_" + signal_region_wp[len("os_"):])
        return DotDict(qcd_regions)

    def get_norm_systematics(self, processes_datasets, region):
        """
        Method to extract all normalization systematics from the KLUB files.
        It considers the processes given by the process_group_name and their parents.
        """
        # systematics
        systematics = {}
        all_signal_names = []
        all_background_names = []
        for p in self.processes:
            if p.isSignal:
                all_signal_names.append(p.get_aux("llr_name")
                    if p.get_aux("llr_name", None) else p.name)
            elif not p.isData:
                all_background_names.append(p.get_aux("llr_name")
                    if p.get_aux("llr_name", None) else p.name)

        from cmt.analysis.systReader import systReader
        syst_folder = "files/systematics/"
        syst = systReader(Task.retrieve_file(self, syst_folder + "systematics_{}.cfg".format(
            self.year)), all_signal_names, all_background_names, None)
        syst.writeOutput(False)
        syst.verbose(False)

        channel = self.get_channel_from_region(region)
        if(channel == "mutau"):
            syst.addSystFile(Task.retrieve_file(self, syst_folder
                + "systematics_mutau_%s.cfg" % self.year))
        elif(channel == "etau"):
            syst.addSystFile(Task.retrieve_file(self, syst_folder
                + "systematics_etau_%s.cfg" % self.year))
        syst.addSystFile(Task.retrieve_file(self, syst_folder + "syst_th.cfg"))
        syst.writeSystematics()
        for isy, syst_name in enumerate(syst.SystNames):
            if "CMS_scale_t" in syst.SystNames[isy] or "CMS_scale_j" in syst.SystNames[isy]:
                continue
            for process in processes_datasets:
                original_process = process
                while True:
                    process_name = (process.get_aux("llr_name")
                        if process.get_aux("llr_name", None) else process.name)
                    if process_name in syst.SystProcesses[isy]:
                        iproc = syst.SystProcesses[isy].index(process_name)
                        systVal = syst.SystValues[isy][iproc]
                        if syst_name not in systematics:
                            systematics[syst_name] = {}
                        systematics[syst_name][original_process.name] = eval(systVal)
                        break
                    elif process.parent_process:
                        process=self.processes.get(process.parent_process)
                    else:
                        break
        return systematics


config = Config("Config", year=2022, runPeriod="preEE", ecm=13.6, lumi_pb=8077)
